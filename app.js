const express = require("express");
const app = express();

var request = require('request');
/*app.get("/", function(request, response){

    response.send("<h2>Hi Express!</h2>");
});*/

//PUT Update Deal
/*
app.put('https://api.hubapi.com/deals/v1/deal/419655418?hapikey=033e0bff-c861-408a-ac6c-50d5f0b9170b', (req, res) => {
    res.send('{' +
        '  "properties": [' +
        '    {' +
        '      "name": "dealStage",' +
        '      "value": "example_stage"' +
        '    }' +
        '  ]' +
        '}');
});
*/


request({ url: 'https://api.hubapi.com/deals/v1/deal/419655418?hapikey=033e0bff-c861-408a-ac6c-50d5f0b9170b',
    method: 'PUT', json: {
        "properties": [
            {
                "name": "dealStage",
                "value": "example_stage"
            }
        ]
    }
});

request({ url: 'https://api.hubapi.com/deals/v1/deal?hapikey=033e0bff-c861-408a-ac6c-50d5f0b9170b',
    method: 'POST', json: {
        "associations": {
            "associatedCompanyIds": [
                1078683017
            ],
            "associatedVids": [
                27136
            ]
        },
        "properties": [
            {
                "value": "Tim's Newer Deal",
                "name": "dealname"
            },
            {
                "value": "appointmentscheduled",
                "name": "dealstage"
            },
            {
                "value": "default",
                "name": "pipeline"
            },
            {
                "value": "24",
                "name": "hubspot_owner_id"
            },
            {
                "value": 1409443200000,
                "name": "closedate"
            },
            {
                "value": "60000",
                "name": "amount"
            },
            {
                "value": "newbusiness",
                "name": "dealtype"
            }
        ]
    }
});



/*request.get('https://api.hubapi.com/deals/v1/deal/419655418?hapikey=033e0bff-c861-408a-ac6c-50d5f0b9170b', function (err, res, body) {
    if (err) {
        console.log("error ", res);
    }
    if (res.statusCode == 200) {
        console.log("success ", body);
    } //etc

});*/


/*//GET Deal (неработает)
app.get("https://api.hubapi.com/deals/v1/deal/paged?hapikey=033e0bff-c861-408a-ac6c-50d5f0b9170b&includeAssociations=true&limit=2&properties=dealname", function (req, res) {
    res.send('id: ' + req.query.id);
    console.log("error ", res);

    //response.send("<h2>Hi Express!</h2>");
});*/

const XeroClient = require('xero-node').AccountingAPIClient;


const config =
    {
        "appType": "private",
        "consumerKey": "14JFSHLFXB3WFOX6LJKIHFBMXVWTOQ",
        "consumerSecret": "LE2IYX6CZYHIATSRRC2RW57NVZ6SG3",
        "privateKeyPath": "privatekey.pem"
    }
;

(async function () {

        let xero = new XeroClient(config);

        // Create new contact:
        await xero.contacts.create({
            Name: 'TestContact'
        });

        // Create new invoice:
        await xero.invoices.create({
            "Type": "ACCREC",
            "Contact": {
                "Name": "TestContact"
            },
            "DueDate": "\/Date(1518685950940+0000)\/",
            "LineItems": [
                {
                    "Description": "Services as agreed",
                    "Quantity": "3",
                    "UnitAmount": "500.00",
                    "AccountCode": "100"
                }
            ],
            "Status": "AUTHORISED"
        });

        const res = await xero.contacts.get();
        console.log("First Contact: ", res.Contacts[0].Name);
    }
)();

//app.listen(3000);